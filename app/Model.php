<?php

namespace App;

use \PDO;
use \PDOException;

abstract class Model
{
    // Set environment
    private $env = "dev"; // `prod` or `dev`
    //Login informations
    private $db = "mysql"; // `mysql` or `pgsql`
    protected $dbname = "oilstore";
    private $host = "";
    private $username = "";
    private $password = "";
    private $port = "5432";
    //Connexion
    protected $_connexion;

    //Id et tanle name
    public $id;
    public $table = "";

    function setLoginInfo()
    {
        if ($this->env === "prod") { // Login infos for prod
            $this->dbname = "epiz_32641247_oilstore";
            $this->username = "epiz_32641247";
            $this->password = "24KZMrl2TdU";
            $this->host = "sql101.epizy.com:3306";
        } else if ($this->env === "dev") {
            $this->dbname = "oilstore";
            $this->username = "senor16";
            $this->password = "senor16";
            if ($this->db == "mysql") {
                $this->host = "localhost:3306";
            } else if ($this->db == "pgsql") {
                $this->host = "localhost";
                $this->port = "5432";
            }
        }
    }

    //Function with connects to the database
    //It returns an PDO object
    public function getConnexion()
    {
        $this->connexion = null;
        $this->setLoginInfo();
        try {
            if ($this->db == "mysql") { //MySQL
                $this->connexion = new PDO("mysql:host=" . $this->host . "; dbname=" . $this->dbname, $this->username, $this->password);
            } else if ($this->db == "pgsql") { //PostgreSQL
                $this->connexion = new PDO("pgsql:host=" . $this->host . ";dbname=" . $this->dbname, $this->username, $this->password);
            }
            $this->connexion->exec("set names utf8");
            $this->connexion->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            die("Erreur Connexion " . $exception->getMessage());
        }
    }

    //Function with get all the records of a table
    //It returns it as object
    public function getAll()
    {
        try {
            $sql = "SELECT * FROM {$this->dbname}.{$this->table} ORDER BY id DESC";
            $query = $this->connexion->prepare($sql);
            $query->execute();
            return $query->fetchAll();
        } catch (\PDOException $exception) {
            die("Error GetAll " . $exception->getMessage());
            return false;
        }
    }

    //Function with get one record on with it's id
    //args : $id the id of the record
    public function getOne($id)
    {
        try {
            $sql = "SELECT * FROM {$this->dbname}.{$this->table} WHERE id = :id";
            $query = $this->connexion->prepare($sql);
            $query->execute(['id' => $id]);
            return $query->fetch();
        } catch (\PDOException $exception) {
            die("Error getOne " . $exception->getMessage());
            return false;
        }
    }

    //Function with get all records ontaining a key in the title or in the content
    //args : $q the key
    public function search(string $q)
    {
        try {
            $sql = "SELECT * FROM {$this->dbname}.{$this->table} ";
            $sql = $sql . " WHERE designation LIKE ('%" . $q . "%')";
            $query = $this->connexion->prepare($sql);
            $query->execute();
            return $query->fetchAll();
        } catch (\PDOException $exception) {
            die("Error search " . $exception->getMessage());
            return false;
        }
    }

    /*
        Delete an record by his id
    */
    public function delete($id)
    {
        try {
            $sql = "DELETE FROM {$this->dbname}.{$this->table} WHERE id=" . $id;
            $query = $this->connexion->prepare($sql);
            $query->execute();
            return true;
        } catch (\PDOException $exception) {
            die('Error deletePress : ' . $exception->getMessage());
            return false;
        }
    }
}
